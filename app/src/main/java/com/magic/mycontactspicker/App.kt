package com.magic.mycontactspicker

import android.app.Application

class App : Application() {
    companion object {
        lateinit var appContext: Application
    }

    override fun onCreate() {
        super.onCreate()
        App.appContext = this
    }

}
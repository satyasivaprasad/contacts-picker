package com.magic.mycontactspicker

import android.app.Activity
import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.util.Log
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ferfalk.simplesearchview.SimpleSearchView
import com.ferfalk.simplesearchview.utils.DimensUtils
import com.magic.mycontactspicker.contacts.ContactsAdapter
import com.magic.mycontactspicker.contacts.ContactsAdapter.OnContactSelectedListener
import com.magic.mycontactspicker.contacts.ContactsHelper
import com.magic.mycontactspicker.contacts.models.Contact
import java.util.ArrayList

class MainActivity : AppCompatActivity() {
    private var contactsListView: RecyclerView? = null
    private var contactsAdapter: ContactsAdapter? = null
    private var searchView: SimpleSearchView? = null
    val EXTRA_REVEAL_CENTER_PADDING = 40

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val toolbar =
            findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)

        searchView = findViewById(R.id.searchView)
        contactsListView = findViewById(R.id.contacts_list)
        ContactsHelper(this).getContacts {
            contactsListView!!.layoutManager = LinearLayoutManager(this)
            contactsAdapter =  ContactsAdapter(it, object : OnContactSelectedListener {
                override fun onContactSelected(contact: Contact) {
                    val selectedContacts: ArrayList<Contact> = ArrayList()
                    selectedContacts.add(contact)
                    val intent = Intent()
                    intent.putParcelableArrayListExtra("selectedContacts", selectedContacts)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            })
            contactsListView!!.adapter =  contactsAdapter
        }

        searchView!!.setOnQueryTextListener(object :
            SimpleSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                Log.d("SimpleSearchView", "Submit:$query")
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                Log.d("SimpleSearchView", "Text changed:$newText")
                if (contactsAdapter != null) {
                    contactsAdapter!!.getFilter().filter(newText)
                }
                return false
            }

            override fun onQueryTextCleared(): Boolean {
                Log.d("SimpleSearchView", "Text cleared")
                return false
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.search, menu)
        setupSearchView(menu!!)
        return true
    }

    override fun onBackPressed() {
        if (searchView!!.onBackPressed()) {
            return
        }
        super.onBackPressed()
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        if (searchView!!.onActivityResult(requestCode, resultCode, data)) {
            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun setupSearchView(menu: Menu) {
        val item = menu.findItem(R.id.action_search)
        searchView!!.setMenuItem(item)
        // Adding padding to the animation because of the hidden menu item
        val revealCenter: Point = searchView!!.getRevealAnimationCenter()
        revealCenter.x -= DimensUtils.convertDpToPx(EXTRA_REVEAL_CENTER_PADDING, this)
    }
}


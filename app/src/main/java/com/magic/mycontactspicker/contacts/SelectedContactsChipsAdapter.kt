package com.magic.mycontactspicker.contacts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.magic.mycontactspicker.R
import com.magic.mycontactspicker.contacts.models.Contact
import kotlinx.android.synthetic.main.selected_contact_chip.view.*
import java.util.ArrayList

class SelectedContactsChipsAdapter(val chipsContents: ArrayList<Contact>?) :
    RecyclerView.Adapter<SelectedContactsChipsAdapter.ChipViewHolder>() {

    inner class ChipViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(chipContent: Contact) {
            with(itemView) {
                contact_item_chip.text = chipContent.getNameToDisplay()
                contact_item_chip.setOnCloseIconClickListener {
                    val index = chipsContents!!.indexOf(chipContent)
                    chipsContents.remove(chipContent)
                    notifyItemRemoved(index)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChipViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.selected_contact_chip, parent, false)
        return ChipViewHolder(v)
    }

    override fun getItemCount() = chipsContents!!.size

    override fun onBindViewHolder(holder: ChipViewHolder, position: Int) =
        holder.bind(chipsContents!![position])
}
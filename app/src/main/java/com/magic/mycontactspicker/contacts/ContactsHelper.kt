package com.magic.mycontactspicker.contacts

import android.accounts.Account
import android.accounts.AccountManager
import android.content.ContentResolver
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.provider.ContactsContract
import android.text.TextUtils
import android.util.Log
import android.util.SparseArray
import com.magic.mycontactspicker.contacts.models.*
import com.simplemobiletools.commons.extensions.getIntValue
import com.simplemobiletools.commons.extensions.getStringValue
import java.util.*
import kotlin.collections.ArrayList

class ContactsHelper(val context: Context) {
    private var displayContactSources = ArrayList<String>()

    private fun getVisibleContactSources(): ArrayList<String> {
        val sources = getAllContactSources()
        return ArrayList(sources).map { it.name }.toMutableList() as ArrayList<String>
    }

    private fun getAllContactSources(): ArrayList<ContactSource> {
        val sources = getDeviceContactSources()
        sources.add(
            ContactSource(
                SMT_PRIVATE,
                SMT_PRIVATE,
                "Phone storage (not visible by other apps)"
            )
        )
        return sources.toMutableList() as ArrayList<ContactSource>
    }

    fun getContacts(
        callback: (ArrayList<Contact>) -> Unit
    ) {
        ensureBackgroundThread {
            val startTime = System.currentTimeMillis()
            val contacts = SparseArray<Contact>()
            displayContactSources = getVisibleContactSources()

            getDeviceContacts(contacts)

            val contactsSize = contacts.size()
            val showOnlyContactsWithNumbers = true
            val tempContacts = ArrayList<Contact>(contactsSize)
            val resultContacts = ArrayList<Contact>(contactsSize)

            (0 until contactsSize).filter {
                if (showOnlyContactsWithNumbers) {
                    val valueAt = contacts.valueAt(it)
                    val phoneNumbers = valueAt.phoneNumbers
                    phoneNumbers.isNotEmpty()
                } else {
                    true
                }
            }.mapTo(tempContacts) {
                contacts.valueAt(it)
            }

            tempContacts.filter { displayContactSources.contains(it.source) }
                .groupBy { it.getNameToDisplay().toLowerCase() }.values.forEach { it ->
                if (it.size == 1) {
                    resultContacts.add(it.first())
                } else {
                    val sorted = it.sortedByDescending { it.getStringToCompare().length }
                    resultContacts.add(sorted.first())
                }
            }

            Contact.sorting = SORT_BY_FIRST_NAME
            Contact.startWithSurname = false
            resultContacts.sort()

            Handler(Looper.getMainLooper()).post {
                val endTime = System.currentTimeMillis() - startTime
                Log.d("TAG", "Contacts Time Taken To Load $endTime")
                callback(resultContacts)
            }
        }
    }

    private fun getContentResolverAccounts(): HashSet<ContactSource> {
        val sources = HashSet<ContactSource>()
        arrayOf(
            ContactsContract.Groups.CONTENT_URI,
            ContactsContract.Settings.CONTENT_URI,
            ContactsContract.RawContacts.CONTENT_URI
        ).forEach {
            fillSourcesFromUri(it, sources)
        }

        return sources
    }

    private fun fillSourcesFromUri(uri: Uri, sources: HashSet<ContactSource>) {
        val projection = arrayOf(
            ContactsContract.RawContacts.ACCOUNT_NAME,
            ContactsContract.RawContacts.ACCOUNT_TYPE
        )

        var cursor: Cursor? = null
        try {
            cursor = context.contentResolver.query(uri, projection, null, null, null)
            if (cursor?.moveToFirst() == true) {
                do {
                    val name =
                        cursor.getStringValue(ContactsContract.RawContacts.ACCOUNT_NAME) ?: ""
                    val type =
                        cursor.getStringValue(ContactsContract.RawContacts.ACCOUNT_TYPE) ?: ""
                    var publicName = name

                    val source = ContactSource(name, type, publicName)
                    sources.add(source)
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
        } finally {
            cursor?.close()
        }
    }

    private fun getDeviceContacts(
        contacts: SparseArray<Contact>
    ) {

        val uri = ContactsContract.Data.CONTENT_URI
        val projection = getContactProjection()

        val selection =
            "${ContactsContract.Data.MIMETYPE} = ? OR ${ContactsContract.Data.MIMETYPE} = ?"
        val selectionArgs = arrayOf(
            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
            ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE
        )
        val sortOrder = getSortString()

        var cursor: Cursor? = null
        try {
            cursor =
                context.contentResolver.query(uri, projection, selection, selectionArgs, sortOrder)
            if (cursor?.moveToFirst() == true) {
                do {
                    val accountName =
                        cursor.getStringValue(ContactsContract.RawContacts.ACCOUNT_NAME) ?: ""
                    val accountType =
                        cursor.getStringValue(ContactsContract.RawContacts.ACCOUNT_TYPE) ?: ""

                    val id = cursor.getIntValue(ContactsContract.Data.RAW_CONTACT_ID)
                    var prefix = ""
                    var firstName = ""
                    var middleName = ""
                    var surname = ""
                    var suffix = ""

                    // ignore names at Organization type contacts
                    if (cursor.getStringValue(ContactsContract.Data.MIMETYPE) == ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE) {
                        prefix =
                            cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.PREFIX)
                                ?: ""
                        firstName =
                            cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME)
                                ?: ""
                        middleName =
                            cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME)
                                ?: ""
                        surname =
                            cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME)
                                ?: ""
                        suffix =
                            cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.SUFFIX)
                                ?: ""
                    }

                    val nickname = ""
                    val photoUri =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.PHOTO_URI)
                            ?: ""
                    val numbers =
                        ArrayList<PhoneNumber>()          // proper value is obtained below
                    val emails = ArrayList<Email>()
                    val addresses = ArrayList<Address>()
                    val contactId = cursor.getIntValue(ContactsContract.Data.CONTACT_ID)
                    val thumbnailUri =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.PHOTO_THUMBNAIL_URI)
                            ?: ""

                    val contact = Contact(
                        id,
                        prefix,
                        firstName,
                        middleName,
                        surname,
                        suffix,
                        nickname,
                        photoUri,
                        numbers,
                        emails,
                        addresses,
                        contactId,
                        thumbnailUri,
                        null,
                        accountName
                    )

                    contacts.put(id, contact)
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
        } finally {
            cursor?.close()
        }

        val phoneNumbers = getPhoneNumbers(null)
        var size = phoneNumbers.size()
        for (i in 0 until size) {
            val key = phoneNumbers.keyAt(i)
            if (contacts[key] != null) {
                val numbers = phoneNumbers.valueAt(i)
                contacts[key].phoneNumbers = numbers
            }
        }

        val nicknames = getNicknames()
        size = nicknames.size()
        for (i in 0 until size) {
            val key = nicknames.keyAt(i)
            contacts[key]?.nickname = nicknames.valueAt(i)
        }

        val emails = getEmails()
        size = emails.size()
        for (i in 0 until size) {
            val key = emails.keyAt(i)
            contacts[key]?.emails = emails.valueAt(i)
        }

        val addresses = getAddresses()
        size = addresses.size()
        for (i in 0 until size) {
            val key = addresses.keyAt(i)
            contacts[key]?.addresses = addresses.valueAt(i)
        }
    }

    private fun getPhoneNumbers(contactId: Int? = null): SparseArray<ArrayList<PhoneNumber>> {
        val phoneNumbers = SparseArray<ArrayList<PhoneNumber>>()
        val uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI
        val projection = arrayOf(
            ContactsContract.Data.RAW_CONTACT_ID,
            ContactsContract.CommonDataKinds.Phone.NUMBER,
            ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER,
            ContactsContract.CommonDataKinds.Phone.TYPE,
            ContactsContract.CommonDataKinds.Phone.LABEL
        )

        val selection =
            if (contactId == null) getSourcesSelection() else "${ContactsContract.Data.RAW_CONTACT_ID} = ?"
        val selectionArgs =
            if (contactId == null) getSourcesSelectionArgs() else arrayOf(contactId.toString())

        var cursor: Cursor? = null
        try {
            cursor = context.contentResolver.query(uri, projection, selection, selectionArgs, null)
            if (cursor?.moveToFirst() == true) {
                do {
                    val id = cursor.getIntValue(ContactsContract.Data.RAW_CONTACT_ID)
                    val number =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.Phone.NUMBER)
                            ?: continue
                    val normalizedNumber =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER)
                            ?: number.normalizeNumber()
                    val type = cursor.getIntValue(ContactsContract.CommonDataKinds.Phone.TYPE)
                    val label =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.Phone.LABEL) ?: ""

                    if (phoneNumbers[id] == null) {
                        phoneNumbers.put(id, ArrayList())
                    }

                    val phoneNumber = PhoneNumber(number, type, label, normalizedNumber)
                    phoneNumbers[id].add(phoneNumber)
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
        } finally {
            cursor?.close()
        }

        return phoneNumbers
    }

    private fun getNicknames(contactId: Int? = null): SparseArray<String> {
        val nicknames = SparseArray<String>()
        val uri = ContactsContract.Data.CONTENT_URI
        val projection = arrayOf(
            ContactsContract.Data.RAW_CONTACT_ID,
            ContactsContract.CommonDataKinds.Nickname.NAME
        )

        val selection = getSourcesSelection(true, contactId != null)
        val selectionArgs = getSourcesSelectionArgs(
            ContactsContract.CommonDataKinds.Nickname.CONTENT_ITEM_TYPE,
            contactId
        )

        var cursor: Cursor? = null
        try {
            cursor = context.contentResolver.query(uri, projection, selection, selectionArgs, null)
            if (cursor?.moveToFirst() == true) {
                do {
                    val id = cursor.getIntValue(ContactsContract.Data.RAW_CONTACT_ID)
                    val nickname =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.Nickname.NAME)
                            ?: continue
                    nicknames.put(id, nickname)
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
        } finally {
            cursor?.close()
        }

        return nicknames
    }

    private fun getEmails(contactId: Int? = null): SparseArray<ArrayList<Email>> {
        val emails = SparseArray<ArrayList<Email>>()
        val uri = ContactsContract.CommonDataKinds.Email.CONTENT_URI
        val projection = arrayOf(
            ContactsContract.Data.RAW_CONTACT_ID,
            ContactsContract.CommonDataKinds.Email.DATA,
            ContactsContract.CommonDataKinds.Email.TYPE,
            ContactsContract.CommonDataKinds.Email.LABEL
        )

        val selection =
            if (contactId == null) getSourcesSelection() else "${ContactsContract.Data.RAW_CONTACT_ID} = ?"
        val selectionArgs =
            if (contactId == null) getSourcesSelectionArgs() else arrayOf(contactId.toString())

        var cursor: Cursor? = null
        try {
            cursor = context.contentResolver.query(uri, projection, selection, selectionArgs, null)
            if (cursor?.moveToFirst() == true) {
                do {
                    val id = cursor.getIntValue(ContactsContract.Data.RAW_CONTACT_ID)
                    val email = cursor.getStringValue(ContactsContract.CommonDataKinds.Email.DATA)
                        ?: continue
                    val type = cursor.getIntValue(ContactsContract.CommonDataKinds.Email.TYPE)
                    val label =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.Email.LABEL) ?: ""

                    if (emails[id] == null) {
                        emails.put(id, ArrayList())
                    }

                    emails[id]!!.add(Email(email, type, label))
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
        } finally {
            cursor?.close()
        }

        return emails
    }

    private fun getAddresses(contactId: Int? = null): SparseArray<ArrayList<Address>> {
        val addresses = SparseArray<ArrayList<Address>>()
        val uri = ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI
        val projection = arrayOf(
            ContactsContract.Data.RAW_CONTACT_ID,
            ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS,
            ContactsContract.CommonDataKinds.StructuredPostal.TYPE,
            ContactsContract.CommonDataKinds.StructuredPostal.LABEL
        )

        val selection =
            if (contactId == null) getSourcesSelection() else "${ContactsContract.Data.RAW_CONTACT_ID} = ?"
        val selectionArgs =
            if (contactId == null) getSourcesSelectionArgs() else arrayOf(contactId.toString())

        var cursor: Cursor? = null
        try {
            cursor = context.contentResolver.query(uri, projection, selection, selectionArgs, null)
            if (cursor?.moveToFirst() == true) {
                do {
                    val id = cursor.getIntValue(ContactsContract.Data.RAW_CONTACT_ID)
                    val address =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS)
                            ?: continue
                    val type =
                        cursor.getIntValue(ContactsContract.CommonDataKinds.StructuredPostal.TYPE)
                    val label =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredPostal.LABEL)
                            ?: ""

                    if (addresses[id] == null) {
                        addresses.put(id, ArrayList())
                    }

                    addresses[id]!!.add(Address(address, type, label))
                } while (cursor.moveToNext())
            }
        } catch (e: Exception) {
        } finally {
            cursor?.close()
        }

        return addresses
    }

    private fun getQuestionMarks() =
        "?,".times(displayContactSources.filter { it.isNotEmpty() }.size).trimEnd(',')

    private fun getSourcesSelection(
        addMimeType: Boolean = false,
        addContactId: Boolean = false,
        useRawContactId: Boolean = true
    ): String {
        val strings = ArrayList<String>()
        if (addMimeType) {
            strings.add("${ContactsContract.Data.MIMETYPE} = ?")
        }

        if (addContactId) {
            strings.add("${if (useRawContactId) ContactsContract.Data.RAW_CONTACT_ID else ContactsContract.Data.CONTACT_ID} = ?")
        } else {
            // sometimes local device storage has null account_name, handle it properly
            val accountnameString = StringBuilder()
            if (displayContactSources.contains("")) {
                accountnameString.append("(")
            }
            accountnameString.append("${ContactsContract.RawContacts.ACCOUNT_NAME} IN (${getQuestionMarks()})")
            if (displayContactSources.contains("")) {
                accountnameString.append(" OR ${ContactsContract.RawContacts.ACCOUNT_NAME} IS NULL)")
            }
            strings.add(accountnameString.toString())
        }

        return TextUtils.join(" AND ", strings)
    }

    private fun getSourcesSelectionArgs(
        mimetype: String? = null,
        contactId: Int? = null
    ): Array<String> {
        val args = ArrayList<String>()

        if (mimetype != null) {
            args.add(mimetype)
        }

        if (contactId != null) {
            args.add(contactId.toString())
        } else {
            args.addAll(displayContactSources.filter { it.isNotEmpty() })
        }

        return args.toTypedArray()
    }

    fun getContactWithLookupKey(key: String): Contact? {
        val selection =
            "(${ContactsContract.Data.MIMETYPE} = ? OR ${ContactsContract.Data.MIMETYPE} = ?) AND ${ContactsContract.Data.LOOKUP_KEY} = ?"
        val selectionArgs = arrayOf(
            ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE,
            ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE,
            key
        )
        return parseContactCursor(selection, selectionArgs)
    }

    private fun parseContactCursor(selection: String, selectionArgs: Array<String>): Contact? {
        val uri = ContactsContract.Data.CONTENT_URI
        val projection = getContactProjection()
        var cursor: Cursor? = null
        try {
            cursor = context.contentResolver.query(uri, projection, selection, selectionArgs, null)
            if (cursor?.moveToFirst() == true) {
                val id = cursor.getIntValue(ContactsContract.Data.RAW_CONTACT_ID)

                var prefix = ""
                var firstName = ""
                var middleName = ""
                var surname = ""
                var suffix = ""

                // ignore names at Organization type contacts
                if (cursor.getStringValue(ContactsContract.Data.MIMETYPE) == ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE) {
                    prefix =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.PREFIX)
                            ?: ""
                    firstName =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME)
                            ?: ""
                    middleName =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME)
                            ?: ""
                    surname =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME)
                            ?: ""
                    suffix =
                        cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.SUFFIX)
                            ?: ""
                }

                val nickname = getNicknames(id)[id] ?: ""
                val photoUri =
                    cursor.getStringValue(ContactsContract.CommonDataKinds.Phone.PHOTO_URI) ?: ""
                val number = getPhoneNumbers(id)[id] ?: ArrayList()
                val emails = getEmails(id)[id] ?: ArrayList()
                val addresses = getAddresses(id)[id] ?: ArrayList()
                val accountName =
                    cursor.getStringValue(ContactsContract.RawContacts.ACCOUNT_NAME) ?: ""
                val starred =
                    cursor.getIntValue(ContactsContract.CommonDataKinds.StructuredName.STARRED)
                val contactId = cursor.getIntValue(ContactsContract.Data.CONTACT_ID)
                val thumbnailUri =
                    cursor.getStringValue(ContactsContract.CommonDataKinds.StructuredName.PHOTO_THUMBNAIL_URI)
                        ?: ""
                return Contact(
                    id,
                    prefix,
                    firstName,
                    middleName,
                    surname,
                    suffix,
                    nickname,
                    photoUri,
                    number,
                    emails,
                    addresses,
                    contactId,
                    thumbnailUri,
                    null,
                    accountName
                )
            }
        } finally {
            cursor?.close()
        }

        return null
    }

    fun getDeviceContactSources(): LinkedHashSet<ContactSource> {
        val sources = LinkedHashSet<ContactSource>()
        val accounts = AccountManager.get(context).accounts
        accounts.forEach {
            if (ContentResolver.getIsSyncable(it, ContactsContract.AUTHORITY) == 1) {
                var publicName = it.name
                val contactSource = ContactSource(it.name, it.type, publicName)
                sources.add(contactSource)
            }
        }

        val contentResolverAccounts = getContentResolverAccounts().filter {
            it.name.isNotEmpty() && it.type.isNotEmpty() && !accounts.contains(
                Account(
                    it.name,
                    it.type
                )
            )
        }
        sources.addAll(contentResolverAccounts)

        return sources
    }

    private fun getContactProjection() = arrayOf(
        ContactsContract.Data.MIMETYPE,
        ContactsContract.Data.CONTACT_ID,
        ContactsContract.Data.RAW_CONTACT_ID,
        ContactsContract.CommonDataKinds.StructuredName.PREFIX,
        ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME,
        ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME,
        ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME,
        ContactsContract.CommonDataKinds.StructuredName.SUFFIX,
        ContactsContract.CommonDataKinds.StructuredName.PHOTO_URI,
        ContactsContract.CommonDataKinds.StructuredName.PHOTO_THUMBNAIL_URI,
        ContactsContract.CommonDataKinds.StructuredName.STARRED,
        ContactsContract.RawContacts.ACCOUNT_NAME,
        ContactsContract.RawContacts.ACCOUNT_TYPE
    )

    private fun getSortString(): String {
        val sorting = SORT_BY_FIRST_NAME
        return when {
            sorting and SORT_BY_FIRST_NAME != 0 -> "${ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME} COLLATE NOCASE"
            sorting and SORT_BY_MIDDLE_NAME != 0 -> "${ContactsContract.CommonDataKinds.StructuredName.MIDDLE_NAME} COLLATE NOCASE"
            sorting and SORT_BY_SURNAME != 0 -> "${ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME} COLLATE NOCASE"
            else -> ContactsContract.CommonDataKinds.Phone.NUMBER
        }
    }

}
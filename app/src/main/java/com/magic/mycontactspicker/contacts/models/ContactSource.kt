package com.magic.mycontactspicker.contacts.models

import com.magic.mycontactspicker.contacts.SMT_PRIVATE

data class ContactSource(var name: String, var type: String, var publicName: String) {
    fun getFullIdentifier(): String {
        return if (type == SMT_PRIVATE) {
            type
        } else {
            "$name:$type"
        }
    }
}

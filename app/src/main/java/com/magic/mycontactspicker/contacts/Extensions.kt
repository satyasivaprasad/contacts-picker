package com.magic.mycontactspicker.contacts

import android.os.Looper
import android.telephony.PhoneNumberUtils

const val SORT_BY_FIRST_NAME = 128
const val SORT_BY_MIDDLE_NAME = 256
const val SORT_BY_SURNAME = 512
const val SORT_DESCENDING = 1024
const val SMT_PRIVATE = "smt_private"   // used at the contact source of local contacts hidden from other apps

val normalizeRegex = "\\p{InCombiningDiacriticalMarks}+".toRegex()
fun String.normalizeNumber() = PhoneNumberUtils.normalizeNumber(this)

fun isOnMainThread() = Looper.myLooper() == Looper.getMainLooper()

fun ensureBackgroundThread(callback: () -> Unit) {
    if (isOnMainThread()) {
        Thread {
            callback()
        }.start()
    } else {
        callback()
    }
}

operator fun String.times(x: Int): String {
    val stringBuilder = StringBuilder()
    for (i in 1..x) {
        stringBuilder.append(this)
    }
    return stringBuilder.toString()
}
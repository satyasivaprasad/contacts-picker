package com.magic.mycontactspicker.contacts.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Address(var value: String, var type: Int, var label: String) : Parcelable

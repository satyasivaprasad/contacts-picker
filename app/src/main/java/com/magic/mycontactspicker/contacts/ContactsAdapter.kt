package com.magic.mycontactspicker.contacts

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.magic.mycontactspicker.App
import com.magic.mycontactspicker.R
import com.magic.mycontactspicker.contacts.models.Contact
import kotlinx.android.synthetic.main.contacts_item.view.*

class ContactsAdapter(private var contactsList: ArrayList<Contact>, private var onContactSelectedListener: OnContactSelectedListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable,
    ContactsSearchFilter.OnResultsPublishedListener {

    private var filter: ContactsSearchFilter? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ContactsViewHolder(LayoutInflater.from(App.appContext).inflate(R.layout.contacts_item, parent, false))
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        Glide.with(App.appContext).clear(holder.itemView.contact_tmb)
    }
    override fun getItemCount(): Int {
        return contactsList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val holder: ContactsViewHolder = holder as ContactsViewHolder
        holder.renderContactView(
            contactsList.get(position)
        )
        holder.itemView.setOnClickListener {
            onContactSelectedListener.onContactSelected(contactsList.get(position))
        }
    }

    override fun getFilter(): Filter {
        if (filter == null) {
            filter = ContactsSearchFilter(contactsList)
            filter!!.setOnResultsPublishedListener(this)
        }
        return filter!!
    }


    override fun onResultsPublished(filteredItems: java.util.ArrayList<Contact?>?) {
        contactsList = filteredItems as ArrayList<Contact>
        notifyDataSetChanged()
    }

    @FunctionalInterface
    interface OnContactSelectedListener {
        fun onContactSelected(contact: Contact)
    }
}
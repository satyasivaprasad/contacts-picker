// Copyright (C) 2017 Peel Inc.
package com.magic.mycontactspicker.contacts

import android.text.TextUtils
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey
import com.magic.mycontactspicker.App
import com.magic.mycontactspicker.R
import com.magic.mycontactspicker.contacts.models.Contact

class ContactsViewHolder(view: View) : ViewHolder(view) {
    private val ivContactImage: CircleImageView
    private val tvContactName: TextView
    private val tvContactNumber: TextView
    //    private final FrameLayout contactPickerParentLayout;
    private val contact: Contact? = null
    private var smallPadding = App.appContext.resources.getDimension(R.dimen.small_margin).toInt()
    private var mediumPadding = App.appContext.resources.getDimension(R.dimen.medium_margin).toInt()
    private var bigPadding = App.appContext.resources.getDimension(R.dimen.normal_margin).toInt()

    fun renderContactView(contact: Contact) {
        tvContactName.text = contact.getNameToDisplay()
        val value = contact.phoneNumbers[0].value
        if (!TextUtils.isEmpty(value)) {
            tvContactNumber.text = value
            tvContactNumber.visibility = View.VISIBLE
        } else {
            tvContactNumber.visibility = View.GONE
        }
        loadUserProfileImageFromLocalContacts(ivContactImage, contact)
    }

    private fun loadUserProfileImageFromLocalContacts(image: ImageView, contact: Contact) {
        image.apply {
            when {
                contact.photoUri.isNotEmpty() -> {
                    val options = RequestOptions()
                        .signature(ObjectKey(contact.photoUri))
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .centerCrop()

                    Glide.with(App.appContext)
                        .load(contact.photoUri)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .apply(options)
                        .apply(RequestOptions.circleCropTransform())
                        .into(image)
                    image.setPadding(smallPadding, smallPadding, smallPadding, smallPadding)
                }
                contact.photo != null -> {
                    val options = RequestOptions()
                        .signature(ObjectKey(contact.hashCode()))
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                        .centerCrop()

                    Glide.with(App.appContext)
                        .load(contact.photo)
                        .transition(DrawableTransitionOptions.withCrossFade())
                        .apply(options)
                        .apply(RequestOptions.circleCropTransform())
                        .into(image)
                    image.setPadding(smallPadding, smallPadding, smallPadding, smallPadding)
                }
                else -> {
                    image.setPadding(mediumPadding, mediumPadding, mediumPadding, mediumPadding)
                    (image as CircleImageView).generateDefaultPicture(contact.getNameToDisplay(), 15F, true)
                }

            }
            image.invalidate()
        }
    }

    init {
        ivContactImage = view.findViewById(R.id.contact_tmb)
        tvContactName = view.findViewById(R.id.contact_name)
        tvContactNumber = view.findViewById(R.id.contact_number)
        //        contactPickerParentLayout = view.findViewById(R.id.contact_frame);
    }
}
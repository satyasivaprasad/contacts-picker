package com.magic.mycontactspicker.contacts

import android.widget.Filter
import com.magic.mycontactspicker.contacts.models.Contact
import java.util.*

class ContactsSearchFilter(private val filterList: List<Contact>) :
    Filter() {
    fun setOnResultsPublishedListener(onResultsPublishedListener: OnResultsPublishedListener?) {
        this.onResultsPublishedListener = onResultsPublishedListener
    }

    private var onResultsPublishedListener: OnResultsPublishedListener? = null
    override fun performFiltering(constraint: CharSequence): FilterResults {
        var constraint: CharSequence? = constraint
        val results = FilterResults()
        if (constraint != null && constraint.length > 0) {
            constraint = constraint.toString().toUpperCase()
            val filteredSelectableContactItems =
                ArrayList<Contact>()
            for (i in filterList.indices) {
                val selectableContactItem = filterList[i]
                val nameToDisplay = selectableContactItem.getNameToDisplay()
                if (nameToDisplay.toUpperCase().contains(constraint) || selectableContactItem.doesContainPhoneNumber(
                        constraint.toString(),
                        true
                    )
                ) {
                    filteredSelectableContactItems.add(selectableContactItem)
                }
            }
            results.count = filteredSelectableContactItems.size
            results.values = filteredSelectableContactItems
        } else {
            results.count = filterList.size
            results.values = filterList
        }
        return results
    }

    override fun publishResults(
        constraint: CharSequence,
        results: FilterResults
    ) {
        if (null != onResultsPublishedListener) {
            onResultsPublishedListener!!.onResultsPublished(results.values as ArrayList<Contact?>)
        }
    }

    interface OnResultsPublishedListener {
        fun onResultsPublished(filteredItems: ArrayList<Contact?>?)
    }

}
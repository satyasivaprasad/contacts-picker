package com.magic.mycontactspicker.contacts

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.TextUtils
import android.util.AttributeSet
import android.util.DisplayMetrics
import android.util.Log
import androidx.annotation.VisibleForTesting
import androidx.appcompat.widget.AppCompatImageView
import com.magic.mycontactspicker.App
import com.magic.mycontactspicker.R
import java.io.FileNotFoundException
import java.io.InputStream

class CircleImageView : AppCompatImageView {
    private val mDrawableRect = RectF()
    private val mBorderRect = RectF()
    private val mShaderMatrix = Matrix()
    private val mBitmapPaint = Paint()
    private val mBorderPaint = Paint()
    private var mBorderColor = DEFAULT_BORDER_COLOR
    private var mBorderWidth = DEFAULT_BORDER_WIDTH
    private var mBitmap: Bitmap? = null
    private var mBitmapShader: BitmapShader? = null
    private var mBitmapWidth = 0
    private var mBitmapHeight = 0
    private var mDrawableRadius = 0f
    private var mBorderRadius = 0f
    private var mReady = false
    private var mSetupPending = false
    private var textPaint: Paint? = null
    private var name: String? = null
    private var isCircular = false

    constructor(context: Context?) : super(context) {}

    enum class TextSizeInDp(val size: Int) {
        SMALL(15), LARGE(72);

    }

    @JvmOverloads
    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int = 0
    ) : super(context, attrs, defStyle) {
        super.setScaleType(SCALE_TYPE)
        val a = context.obtainStyledAttributes(
            attrs,
            R.styleable.CircleImageView,
            defStyle,
            0
        )
        mBorderWidth = a.getDimensionPixelSize(
            R.styleable.CircleImageView_border_width,
            DEFAULT_BORDER_WIDTH
        )
        mBorderColor = a.getColor(
            R.styleable.CircleImageView_border_color,
            DEFAULT_BORDER_COLOR
        )
        a.recycle()
        mReady = true
        if (mSetupPending) {
            setup()
            mSetupPending = false
        }
    }

    override fun getScaleType(): ScaleType {
        return SCALE_TYPE
    }

    override fun setScaleType(scaleType: ScaleType) {
        require(scaleType == SCALE_TYPE) {
            String.format(
                "ScaleType %s not supported.",
                scaleType
            )
        }
    }

    override fun onDraw(canvas: Canvas) {
        if (drawable == null) {
            if (!TextUtils.isEmpty(name)) {
                val paint = Paint()
                paint.style = Paint.Style.FILL
                paint.color = Color.parseColor("#EFEFF4")
                if (isCircular) {
                    canvas.drawCircle(
                        width / 2.toFloat(),
                        height / 2.toFloat(),
                        width / 2.toFloat(),
                        paint
                    )
                } else {
                    canvas.drawPaint(paint)
                }
                val xPos = canvas.width / 2
                val yPos =
                    (canvas.height / 2 - (textPaint!!.descent() + textPaint!!.ascent()) / 2).toInt()
                canvas.drawText(name!!, xPos.toFloat(), yPos.toFloat(), textPaint!!)
            }
            return
        }
        if (isCircular) {
            canvas.drawCircle(
                width / 2.toFloat(),
                height / 2.toFloat(),
                mDrawableRadius,
                mBitmapPaint
            )
            canvas.drawCircle(
                width / 2.toFloat(),
                height / 2.toFloat(),
                mBorderRadius,
                mBorderPaint
            )
        } else {
            canvas.drawBitmap(mBitmap!!, left.toFloat(), top.toFloat(), mBitmapPaint)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        setup()
    }

    var borderColor: Int
        get() = mBorderColor
        set(borderColor) {
            if (borderColor == mBorderColor) {
                return
            }
            mBorderColor = borderColor
            mBorderPaint.color = mBorderColor
            invalidate()
        }

    var borderWidth: Int
        get() = mBorderWidth
        set(borderWidth) {
            if (borderWidth == mBorderWidth) {
                return
            }
            mBorderWidth = borderWidth
            setup()
        }

    override fun setImageBitmap(bm: Bitmap) {
        super.setImageBitmap(bm)
        mBitmap = bm
        setup()
    }

    override fun setImageURI(uri: Uri?) {
        super.setImageURI(uri)
        mBitmap = getBitmapFromURI(uri)
        setup()
    }

    private fun getBitmapFromURI(uri: Uri?): Bitmap? {
        var getBitmap: Bitmap? = null
        try {
            val imageStream: InputStream?
            try {
                imageStream = App.appContext.contentResolver.openInputStream(uri!!)
                getBitmap = BitmapFactory.decodeStream(imageStream)
            } catch (e: FileNotFoundException) {
                Log.e("", e.message)
            }
        } catch (e: Exception) {
            Log.e("", e.message)
        }
        return getBitmap
    }

    override fun setImageDrawable(drawable: Drawable?) {
        super.setImageDrawable(drawable)
        mBitmap = getBitmapFromDrawable(drawable)
        setup()
    }

    override fun setImageResource(resId: Int) {
        super.setImageResource(resId)
        mBitmap = getBitmapFromDrawable(drawable)
        setup()
    }

    private fun getBitmapFromDrawable(drawable: Drawable?): Bitmap? {
        if (drawable == null) {
            return null
        }
        return if (drawable is BitmapDrawable) {
            drawable.bitmap
        } else try {
            val bitmap: Bitmap
            bitmap = if (drawable is ColorDrawable) {
                Bitmap.createBitmap(
                    COLORDRAWABLE_DIMENSION,
                    COLORDRAWABLE_DIMENSION,
                    BITMAP_CONFIG
                )
            } else {
                Bitmap.createBitmap(
                    drawable.intrinsicWidth,
                    drawable.intrinsicHeight,
                    BITMAP_CONFIG
                )
            }
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.width, canvas.height)
            drawable.draw(canvas)
            bitmap
        } catch (e: OutOfMemoryError) {
            null
        }
    }

    private fun setup() {
        if (!mReady) {
            mSetupPending = true
            return
        }
        if (mBitmap == null) {
            return
        }
        mBitmapShader = BitmapShader(
            mBitmap!!,
            Shader.TileMode.CLAMP,
            Shader.TileMode.CLAMP
        )
        mBitmapPaint.isAntiAlias = true
        mBitmapPaint.shader = mBitmapShader
        mBorderPaint.style = Paint.Style.STROKE
        mBorderPaint.isAntiAlias = true
        mBorderPaint.color = mBorderColor
        mBorderPaint.strokeWidth = mBorderWidth.toFloat()
        mBitmapHeight = mBitmap!!.height
        mBitmapWidth = mBitmap!!.width
        mBorderRect[0f, 0f, width.toFloat()] = height.toFloat()
        mBorderRadius = Math.min(
            (mBorderRect.height() - mBorderWidth) / 2,
            (mBorderRect.width() - mBorderWidth) / 2
        )
        mDrawableRect[mBorderWidth.toFloat(), mBorderWidth.toFloat(), mBorderRect.width() - mBorderWidth] =
            mBorderRect.height() - mBorderWidth
        mDrawableRadius = Math.min(mDrawableRect.height() / 2, mDrawableRect.width() / 2)
        updateShaderMatrix()
        invalidate()
    }

    private fun updateShaderMatrix() {
        val scale: Float
        var dx = 0f
        var dy = 0f
        mShaderMatrix.set(null)
        if (mBitmapWidth * mDrawableRect.height() > mDrawableRect.width() * mBitmapHeight) {
            scale = mDrawableRect.height() / mBitmapHeight.toFloat()
            dx = (mDrawableRect.width() - mBitmapWidth * scale) * 0.5f
        } else {
            scale = mDrawableRect.width() / mBitmapWidth.toFloat()
            dy = (mDrawableRect.height() - mBitmapHeight * scale) * 0.5f
        }
        mShaderMatrix.setScale(scale, scale)
        mShaderMatrix.postTranslate(
            (dx + 0.5f).toInt() + mBorderWidth.toFloat(),
            (dy + 0.5f).toInt() + mBorderWidth.toFloat()
        )
        mBitmapShader!!.setLocalMatrix(mShaderMatrix)
    }

    fun generateDefaultPicture(
        name: String?,
        sizeInDp: Float,
        isCircular: Boolean
    ) {
        this.isCircular = isCircular
        this.name = calculateAbbreviatedPictureName(name)
        textPaint = Paint()
        textPaint!!.color = Color.parseColor("#666666")
        textPaint!!.textAlign = Paint.Align.CENTER
        textPaint!!.isAntiAlias = true
        textPaint!!.setShadowLayer(1f, 1f, 1f, Color.parseColor("#80000000"))
        textPaint!!.textSize = convertDpToPixel(sizeInDp, context)
    }

    companion object {
        private val SCALE_TYPE =
            ScaleType.CENTER_CROP
        private val BITMAP_CONFIG =
            Bitmap.Config.ARGB_8888
        private const val COLORDRAWABLE_DIMENSION = 1
        private const val DEFAULT_BORDER_WIDTH = 0
        private const val DEFAULT_BORDER_COLOR = Color.TRANSPARENT
        // TextUtils.isDigitsOnly - fails with the tests - but works when copied here.
// So the tests will pass, it is copied here
        private fun isDigitsOnly(str: CharSequence): Boolean {
            val len = str.length
            var cp: Int
            var i = 0
            while (i < len) {
                cp = Character.codePointAt(str, i)
                if (!Character.isDigit(cp)) {
                    return false
                }
                i += Character.charCount(cp)
            }
            return true
        }

        @VisibleForTesting
        fun calculateAbbreviatedPictureName(name: String?): String {
            var result: String
            try {
                val nameWithoutSpaces =
                    name?.replace("[\\s\\(\\)\\-\\+]".toRegex(), "") ?: ""
                if (TextUtils.isEmpty(name) || isDigitsOnly(
                        nameWithoutSpaces
                    )
                ) {
                    result = "#"
                } else {
                    var adjustedName = name!!.replace(
                        "\\s+".toRegex(),
                        " "
                    ) // make multiple whitespace into a single space
                    adjustedName =
                        adjustedName.trim { it <= ' ' } // remove leading/trailing spaces
                    val splits =
                        adjustedName.split("\\s").toTypedArray()
                    if (splits.size > 1) {
                        result = String.format(
                            "%s%s",
                            getFirstCharacter(splits[0].trim { it <= ' ' }),
                            getFirstCharacter(splits[1].trim { it <= ' ' })
                        ).toUpperCase()
                    } else if (splits.size == 1) {
                        if (adjustedName.length > 1) {
                            val firstChar = name[0]
                            val secondChar = adjustedName[1]
                            var count = 0
                            count += if (isSpecialSymbol(firstChar)) {
                                2
                            } else {
                                1
                            }
                            count += if (isSpecialSymbol(secondChar)) {
                                2
                            } else {
                                1
                            }
                            result = if (adjustedName.length > count) adjustedName.substring(
                                0,
                                count
                            ).toUpperCase() else adjustedName.toUpperCase()
                        } else if (adjustedName.length == 1) {
                            result = adjustedName.toUpperCase()
                        } else {
                            result = "#"
                        }
                    } else {
                        result = "#"
                    }
                }
            } catch (e: Exception) { // we have unit tests for all common names
// if we cannot parse the name for some reason, we will return "#"
                result = "#"
            }
            return result
        }

        fun convertDpToPixel(dp: Float, context: Context): Float {
            val resources = context.resources
            val metrics = resources.displayMetrics
            return dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
        }

        private fun getFirstCharacter(string: String): String {
            val first: String
            val c = string[0]
            first = if (isSpecialSymbol(c)) {
                string.substring(0, 2)
            } else {
                c.toString()
            }
            return first
        }

        private fun isSpecialSymbol(c: Char): Boolean {
            return Character.getType(c) == Character.SURROGATE.toInt() || Character.getType(
                c
            ) == Character.OTHER_LETTER.toInt()
        }
    }
}
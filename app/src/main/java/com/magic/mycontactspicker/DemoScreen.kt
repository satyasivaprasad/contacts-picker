package com.magic.mycontactspicker

import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration
import com.google.android.material.snackbar.Snackbar
import com.magic.mycontactspicker.contacts.SelectedContactsChipsAdapter
import com.magic.mycontactspicker.contacts.models.Contact
import kotlinx.android.synthetic.main.activity_demo_screen.*

class DemoScreen : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo_screen)
        setSupportActionBar(toolbar)

        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            startActivityForResult(Intent(this, MainActivity::class.java), 100)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                val selectedContacts =
                    data!!.getParcelableArrayListExtra<Contact>("selectedContacts")
                val chipRecyclerView =
                    findViewById(R.id.selected_contacts_chips_list) as RecyclerView

                val chipsLayoutManager = ChipsLayoutManager.newBuilder(this)
                    .setChildGravity(Gravity.NO_GRAVITY)
                    .setScrollingEnabled(true)
                    .setGravityResolver { Gravity.NO_GRAVITY }
                    .setOrientation(ChipsLayoutManager.HORIZONTAL)
                    .setRowStrategy(ChipsLayoutManager.STRATEGY_DEFAULT)
                    .build()
                chipRecyclerView.layoutManager = chipsLayoutManager

                chipRecyclerView.addItemDecoration(
                    SpacingItemDecoration(
                        resources.getDimensionPixelOffset(R.dimen.chips_horizontal_margin),
                        resources.getDimensionPixelOffset(R.dimen.chips_vertical_margin)
                    )
                )

                chipRecyclerView.adapter = SelectedContactsChipsAdapter(selectedContacts)
            }
        }
    }
}
